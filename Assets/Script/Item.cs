using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Item : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler , IDropHandler
{
    private int id;
    private string name;
    [SerializeField] public ItemScriptable scriptItem;
    [SerializeField] private Image spriteItem;
    [SerializeField] CanvasGroup canvasItem;
    [SerializeField] Recip recipRef;
    [SerializeField] GameObject ObjectDropRef;

    void Start()
    {
        recipRef = GetComponentInParent<Recip>();
        if (scriptItem != null)
        {
            id = scriptItem.Id;
            name = scriptItem.Name;
            spriteItem.sprite = scriptItem.SpriteItem;
        }
    }

    public void SetItem(ItemScriptable itemsScriptToSet)
    {
        scriptItem = itemsScriptToSet;
        id = scriptItem.Id;
        name = scriptItem.Name;
        spriteItem.sprite = scriptItem.SpriteItem;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        canvasItem.blocksRaycasts = false;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        gameObject.transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        
        canvasItem.blocksRaycasts = true;
    }

    public void OnDrag(PointerEventData eventData)
    {

        gameObject.transform.position = Input.mousePosition;
    }

    public void OnDrop(PointerEventData eventData)
    {
        ObjectDropRef = eventData.pointerDrag;
        canvasItem.blocksRaycasts = true;
        if (eventData.pointerDrag.CompareTag("Craftable"))
        {
            Item itemOnDrop = ObjectDropRef.GetComponent<Item>();
            CanCreate(itemOnDrop);
        }
        
    }

    public void CanCreate(Item itemOnDrop)
    {
        ItemScriptable craft = recipRef.IsCraftable(itemOnDrop.scriptItem, this.scriptItem);
        if (craft != null)
        {
            GameObject toSpawn = Instantiate(recipRef.itemSpawnable,transform.position,transform.rotation,recipRef.transform);
            Item ItemSpawn = toSpawn.GetComponent<Item>();
            ItemSpawn.SetItem(craft);
            Destroy(ObjectDropRef);
            Destroy(gameObject);
        }
        
    }
}
