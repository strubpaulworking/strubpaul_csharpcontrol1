using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recip : MonoBehaviour
{

    [SerializeField] public List<RecipeScriptable> RecetteList = new List<RecipeScriptable>();
    [SerializeField] public GameObject itemSpawnable;

    public ItemScriptable IsCraftable(ItemScriptable item1,ItemScriptable item2)
    {
       foreach(RecipeScriptable r in RecetteList)
        {
            Debug.LogWarning("item1 vaut " + item1.Id + " | item2 vaut " + item2.Id + " | r.item1 vaut " + r.Item1.Id + " | r.item2 vaut " + r.Item2.Id);
            if (r.Item1.Id==item1.Id || r.Item1.Id==item2.Id)
            {
                if(r.Item2.Id==item1.Id || r.Item2.Id==item2.Id)
                {
                    return r.Result;
                }
            }
        }
        return null;
    }

}
