using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/ItemScriptableObject", order = 1)]
public class ItemScriptable : ScriptableObject
{
    [SerializeField] private int id;
    [SerializeField] private string name;
    [SerializeField] private Sprite spriteItem;

    public int Id
    {
        get { return id; }
    }
    public string Name
    {
        get { return name; }
    }
    public Sprite SpriteItem
    {
        get { return spriteItem; }
    }
}
