using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/RecipeScriptableObject", order = 2)]
public class RecipeScriptable : ScriptableObject
{
    [SerializeField] private ItemScriptable item1;
    [SerializeField] private ItemScriptable item2;
    [SerializeField] private ItemScriptable result;

    public ItemScriptable Item1
    {
        get { return item1; }
    }
    public ItemScriptable Item2
    {
        get { return item2; }
    }
    public ItemScriptable Result
    {
        get { return result; }
    }
}
